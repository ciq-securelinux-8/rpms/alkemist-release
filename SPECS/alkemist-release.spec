Name: alkemist-release

# Version is based on the EL it is part of
Version: 8.0
Release: 1%{?dist}
Summary: Alkemist packages for Rocky Linux
License: Proprietary

Source0: CIQ-Alkemist.repo

BuildArch: noarch

Requires: rocky-release
Provides: alkemist-release = 8

%description
This package contains the DNF repo files for RunSafe Alkemist

%install
install -D -m 644 %{SOURCE0} %{buildroot}%{_sysconfdir}/yum.repos.d/CIQ-Alkemist.repo

%files
%defattr(-,root,root)
%config(noreplace) %{_sysconfdir}/yum.repos.d/CIQ-Alkemist.repo

%changelog
* Tue Feb 8 2022 Mustafa Gezen <mustafa@ciq.co> - 8.0-1
- Introduce alkemist-release
